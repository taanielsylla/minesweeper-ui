import {autoinject} from 'aurelia-framework';
import {IGameDataView} from "../domain/view/IGameDataView";
import {IConfigurationView} from "../domain/view/IConfigurationView";
import {IStateView} from "../domain/view/IStateView";
import {IWalletView} from "../domain/view/IWalletView";

export const ENUM = {
    GAME_DATA: "GAME_DATA",
    CONFIGURATION: "CONFIGURATION",
    GAME_MODE: "GAME_MODE",
    CURRENCY_TYPE: "CURRENCY_TYPE",

    ACTIVE: "ACTIVE",
    RESOLVED: "RESOLVED",
    NO_GAME: "NO_GAME",
}

@autoinject
export class AppState {

    balance: number
    configurationId: string
    currencyType: string
    currentLevel: number
    nickName: string
    roundStatus: string
    totalWin: number
    win: number
    wallets: IWalletView[]

    constructor() {
    }

    // public readonly BASE_URL = 'http://minesweeper2020v1.azurewebsites.net:80/api/v1.0/';
    public readonly BASE_URL = 'https://minesweeper-api.cap.taanielsylla.com:80/api/v1.0/';

    // JSON Web Token to keep track of logged in status
    // https://developer.mozilla.org/en-US/docs/Web/API/Window/localStorage
    get JWT(): string | null {
        return localStorage.getItem('JWT');
    }

    set JWT(value: string | null) {
        if (value) {
            localStorage.setItem('JWT', value);
        } else {
            localStorage.removeItem('JWT');
        }
    }

    isLoggedIn = () => this.JWT != null;

    // Game Data
    set gameData(gameData: IGameDataView) {
        localStorage.setItem(ENUM.GAME_DATA, JSON.stringify(gameData));
    }

    get gameData(): IGameDataView | null {
        if (!localStorage.getItem(ENUM.GAME_DATA)) return null;
        return JSON.parse(localStorage.getItem(ENUM.GAME_DATA)) as IGameDataView;
    }

    // Configuration
    set selectedConfiguration(config: IConfigurationView) {
        localStorage.setItem(ENUM.CONFIGURATION, JSON.stringify(config));
    }

    get selectedConfiguration(): IConfigurationView | null {
        if (!localStorage.getItem(ENUM.CONFIGURATION)) return null;

        const asd = localStorage.getItem(ENUM.GAME_DATA)

        return JSON.parse(localStorage.getItem(ENUM.CONFIGURATION)) as IConfigurationView;
    }


    // Game Mode
    set selectedGameMode(mode: string) {
        localStorage.setItem(ENUM.GAME_MODE, mode);
    }

    get selectedGameMode(): string | null {
        if (!localStorage.getItem(ENUM.GAME_MODE)) return null;
        return localStorage.getItem(ENUM.GAME_MODE);
    }

    // Currency
    set selectedCurrency(config: string) {
        localStorage.setItem(ENUM.CURRENCY_TYPE, config);
    }

    get selectedCurrency(): string | null {
        if (!localStorage.getItem(ENUM.CURRENCY_TYPE)) return null;
        return localStorage.getItem(ENUM.CURRENCY_TYPE);
    }

    parseUserState(data: IStateView) {
        const {balance, configurationId, currencyType, currentLevel, nickName, status, totalWin, win, wallets} = data;
        this.balance = balance;
        this.configurationId = configurationId;
        this.currencyType = currencyType;
        this.currentLevel = currentLevel;
        this.nickName = nickName;
        this.roundStatus = status;
        this.totalWin = totalWin;
        this.win = win;
        this.wallets = wallets
    }
}
