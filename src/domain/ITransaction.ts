export interface ITransaction {

    id?: string;

    appUserId?: string;

    currencyTypeId?: string;

    transactionStatusId?: string;

    transactionTypeId?: string;

    amount?: number;

    endingBalance?: number;
}
