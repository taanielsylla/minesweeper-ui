export interface IAffiliate {

    id?: string;

    appUserId?: string; // user who referred

    referredTo?: string; // ID of user who was referred
}
