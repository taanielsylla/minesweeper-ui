export interface IAppUser {

    id?: string;

    email?: string;

    userName?: string;

    firstName: string;

    lastName: string;

    nickname?: string;
}
