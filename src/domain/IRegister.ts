export interface IRegister {

    email?: string;

    password?: string;

    firstName?: string;

    lastName?: string;

    nickName?: string;
}
