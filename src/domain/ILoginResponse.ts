export interface ILoginResponse {

    token: string,

    status: string,

    nickname?: string;
}
