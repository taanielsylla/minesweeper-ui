export interface IWallet {

    id?: string;

    appUserId?: string;

    currencyTypeId?: string;

    amount?: number;
}
