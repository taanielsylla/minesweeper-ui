export interface IGameMode {

    id?: string;

    name?: string;

    countOfLevels?: number;
}
