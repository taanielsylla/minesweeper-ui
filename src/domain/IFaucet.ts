export interface IFaucet {

    id?: string;

    appUserId?: string;

    currencyTypeId?: string;

    amountRetrieved?: number;
}
