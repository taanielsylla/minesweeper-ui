export interface IGameHistory {

    id?: string;

    appUserId?: string;

    gameRoundId?: string;

    bet?: number;

    win?: number
}
