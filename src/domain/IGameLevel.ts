export interface IGameLevel {

    id?: string;

    gameModeId?: string;

    bet?: number;

    levelNr?: number;

    boxCount?: number;
}
