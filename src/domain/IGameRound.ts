export interface IGameRound {

    id?: string;

    appUserId?: string;

    roundStatusId?: string;

    configurationId?: string;

    currentLevel?: number;
}
