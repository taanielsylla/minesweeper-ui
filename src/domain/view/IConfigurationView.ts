export interface IConfigurationView {

    id?: string;

    name?: string;

    gameMode?: string;

    boxCount?: number;

    bets?: number[];

    levelCount?: number

    currencyType?: string
}
