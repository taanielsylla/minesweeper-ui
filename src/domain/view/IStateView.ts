import {IWalletView} from "./IWalletView";

export interface IStateView {

    nickName?: string;

    status?: string;

    balance?: number;

    currencyType?: string;

    configurationId?: string;

    currentLevel?: number;

    win?: number;

    totalWin?: number;

    wallets?: IWalletView[];
}
