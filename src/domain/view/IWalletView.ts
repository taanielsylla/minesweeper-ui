export interface IWalletView {

    currencyName?: string;

    amount?: number;
}
