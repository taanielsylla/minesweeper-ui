import {IConfigurationView} from "./IConfigurationView";
import {IGameHistoryView} from "./IGameHistoryView";

export interface IGameDataView {

    gameModes?: string[];

    currencies?: string[];

    configurations?: IConfigurationView[];

    gameHistory?: IGameHistoryView[];
}
