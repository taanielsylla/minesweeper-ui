export interface IGameHistoryView {

    nickName?: string;

    bet?: number;

    win?: number;
}
