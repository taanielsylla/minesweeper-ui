export interface ITransactionType {

    id?: string;

    name: string;
}
