export interface IConfiguration {

    id?: string;

    currencyTypeId?: string;

    name?: string;

    gameModeId?: string;
}
