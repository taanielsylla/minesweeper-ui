import {autoinject} from 'aurelia-framework';
import {RouteConfig, NavigationInstruction, Router} from 'aurelia-router';
import {IAlertData} from 'types/IAlertData';
import {AlertType} from 'types/AlertType';
import {GameModeService} from "../../services/game-mode-service";

@autoinject
export class GameModesCreate {
    private _alert: IAlertData | null = null;

    private name?: string;
    private countOfLevels?: number;

    private router: Router;

    constructor(private gameModeService: GameModeService, router: Router) {
        this.router = router;
    }

    async formSubmitted(event: Event) {
        await this.gameModeService
            .create({
                name: this.name,
                countOfLevels: this.countOfLevels,
            })
            .then(response => {
                    if (response.statusCode >= 200 && response.statusCode < 300) {
                        this._alert = null;
                        this.router.navigateToRoute('gameModes-index', {});
                    } else {
                        // show error message
                        this._alert = {
                            message: response.statusCode.toString() + ' - ' + response.errorMessage,
                            type: AlertType.Danger,
                            dismissable: true,
                        }
                    }
                }
            );

        event.preventDefault();
    }
}
