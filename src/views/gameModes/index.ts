import {autoinject} from 'aurelia-framework';
import {AlertType} from '../../types/AlertType';
import {IAlertData} from 'types/IAlertData';
import {IGameMode} from "../../domain/IGameMode";
import {GameModeService} from "../../services/game-mode-service";

@autoinject
export class GameModesIndex {
    private _alert: IAlertData | null = null;

    private gameModes: IGameMode[] = [];

    constructor(private gameModeService: GameModeService) {

    }

    async attached() {
        await this.gameModeService.getAll()

            .then(response => {
                    if (response.statusCode >= 200 && response.statusCode < 300) {
                        this._alert = null;
                        this.gameModes = response.data;
                    } else {
                        // show error message
                        this._alert = {
                            message: response.statusCode.toString() + ' - ' + response.errorMessage,
                            type: AlertType.Danger,
                            dismissable: true,
                        }
                    }
                }
            );
    }

}
