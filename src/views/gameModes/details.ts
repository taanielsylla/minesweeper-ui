import {autoinject} from 'aurelia-framework';
import {RouteConfig, NavigationInstruction} from 'aurelia-router';
import {IAlertData} from 'types/IAlertData';
import {AlertType} from 'types/AlertType';
import {IGameMode} from "../../domain/IGameMode";
import {GameModeService} from "../../services/game-mode-service";

@autoinject
export class GameModesDetails {
    private _alert: IAlertData | null = null;

    private gameMode?: IGameMode;

    constructor(private gameModeService: GameModeService) {

    }

    async activate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
        if (params.id && typeof (params.id) == 'string') {

            return await this.gameModeService.getById(params.id).then(
                response => {
                    if (response.statusCode >= 200 && response.statusCode < 300) {
                        this._alert = null;
                        this.gameMode = response.data!;
                    } else {
                        // show error message
                        this._alert = {
                            message: response.statusCode.toString() + ' - ' + response.errorMessage,
                            type: AlertType.Danger,
                            dismissable: true,
                        }
                        this.gameMode = undefined;
                    }
                }
            );
        }
    }

}
