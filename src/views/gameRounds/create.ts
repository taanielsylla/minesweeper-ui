import {autoinject} from 'aurelia-framework';
import {RouteConfig, NavigationInstruction, Router} from 'aurelia-router';
import {IAlertData} from 'types/IAlertData';
import {AlertType} from 'types/AlertType';
import {GameRoundService} from "../../services/game-round-service";

@autoinject
export class CurrencyTypesCreate {
    private _alert: IAlertData | null = null;

    private appUserId?: string;
    private roundStatusId?: string;
    private configurationId?: string;
    private currentLevel?: number;

    private router: Router;

    constructor(private gameRoundService: GameRoundService, router: Router) {
        this.router = router;
    }

    async formSubmitted(event: Event) {
        await this.gameRoundService
            .create({
                appUserId: this.appUserId,
                roundStatusId: this.roundStatusId,
                configurationId: this.configurationId,
                currentLevel: this.currentLevel,
            })
            .then(response => {
                    if (response.statusCode >= 200 && response.statusCode < 300) {
                        this._alert = null;
                        this.router.navigateToRoute('gameRounds-index', {});
                    } else {
                        // show error message
                        this._alert = {
                            message: response.statusCode.toString() + ' - ' + response.errorMessage,
                            type: AlertType.Danger,
                            dismissable: true,
                        }
                    }
                }
            );

        event.preventDefault();
    }
}
