import {autoinject} from 'aurelia-framework';
import {RouteConfig, NavigationInstruction} from 'aurelia-router';
import {IAlertData} from 'types/IAlertData';
import {AlertType} from 'types/AlertType';
import {IGameRound} from "../../domain/IGameRound";
import {GameRoundService} from "../../services/game-round-service";

@autoinject
export class CurrencyTypesDetails {
    private _alert: IAlertData | null = null;

    private gameRound?: IGameRound;

    constructor(private gameRoundService: GameRoundService) {

    }

    async activate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
        if (params.id && typeof (params.id) == 'string') {

            return await this.gameRoundService.getById(params.id).then(
                response => {
                    if (response.statusCode >= 200 && response.statusCode < 300) {
                        this._alert = null;
                        this.gameRound = response.data!;
                    } else {
                        // show error message
                        this._alert = {
                            message: response.statusCode.toString() + ' - ' + response.errorMessage,
                            type: AlertType.Danger,
                            dismissable: true,
                        }
                        this.gameRound = undefined;
                    }
                }
            );
        }
    }

}
