import {autoinject} from 'aurelia-framework';
import {AlertType} from '../../types/AlertType';
import {IAlertData} from 'types/IAlertData';
import {IGameRound} from "../../domain/IGameRound";
import {GameRoundService} from "../../services/game-round-service";

@autoinject
export class GameRoundsIndex {
    private _alert: IAlertData | null = null;

    private gameRounds: IGameRound[] = [];

    constructor(private gameRoundService: GameRoundService) {

    }

    async attached() {
        await this.gameRoundService.getAll()

            .then(response => {
                    if (response.statusCode >= 200 && response.statusCode < 300) {
                        this._alert = null;
                        this.gameRounds = response.data;
                    } else {
                        // show error message
                        this._alert = {
                            message: response.statusCode.toString() + ' - ' + response.errorMessage,
                            type: AlertType.Danger,
                            dismissable: true,
                        }
                    }
                }
            );
    }

}
