import {autoinject} from 'aurelia-framework';
import {AlertType} from '../../types/AlertType';
import {IAlertData} from 'types/IAlertData';
import {IGameHistory} from "../../domain/IGameHistory";
import {GameHistoryService} from "../../services/game-history-service";

@autoinject
export class GameHistoriesIndex {
    private _alert: IAlertData | null = null;

    private gameHistories: IGameHistory[] = [];

    constructor(private gameHistoryService: GameHistoryService) {

    }

    async attached() {
        await this.gameHistoryService.getAll()

            .then(response => {
                    if (response.statusCode >= 200 && response.statusCode < 300) {
                        this._alert = null;
                        this.gameHistories = response.data;
                    } else {
                        // show error message
                        this._alert = {
                            message: response.statusCode.toString() + ' - ' + response.errorMessage,
                            type: AlertType.Danger,
                            dismissable: true,
                        }
                    }
                }
            );
    }

}
