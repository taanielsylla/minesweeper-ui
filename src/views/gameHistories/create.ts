import {autoinject} from 'aurelia-framework';
import {RouteConfig, NavigationInstruction, Router} from 'aurelia-router';
import {IAlertData} from 'types/IAlertData';
import {AlertType} from 'types/AlertType';
import {GameHistoryService} from "../../services/game-history-service";

@autoinject
export class GameHistoriesCreate {
    private _alert: IAlertData | null = null;

    private appUserId?: string;
    private gameRoundId?: string;
    private bet?: number;
    private win?: number;

    private router: Router;

    constructor(private gameHistoryService: GameHistoryService, router: Router) {
        this.router = router;
    }

    async formSubmitted(event: Event) {
        await this.gameHistoryService
            .create({
                appUserId: this.appUserId,
                gameRoundId: this.gameRoundId,
                bet: this.bet,
                win: this.win,
            })
            .then(response => {
                    if (response.statusCode >= 200 && response.statusCode < 300) {
                        this._alert = null;
                        this.router.navigateToRoute('gameHistories-index', {});
                    } else {
                        // show error message
                        this._alert = {
                            message: response.statusCode.toString() + ' - ' + response.errorMessage,
                            type: AlertType.Danger,
                            dismissable: true,
                        }
                    }
                }
            );

        event.preventDefault();
    }
}
