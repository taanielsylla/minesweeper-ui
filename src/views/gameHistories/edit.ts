import {autoinject} from 'aurelia-framework';
import {RouteConfig, NavigationInstruction, Router} from 'aurelia-router';
import {IAlertData} from 'types/IAlertData';
import {AlertType} from 'types/AlertType';
import {IGameHistory} from "../../domain/IGameHistory";
import {GameHistoryService} from "../../services/game-history-service";

@autoinject
export class GameHistoriesEdit {
    private _alert: IAlertData | null = null;

    router?: Router;
    private gameHistory?: IGameHistory;

    constructor(private gameHistoryService: GameHistoryService, router: Router) {
        this.router = router;
    }

    // can bind params in this lifecycle hook
    activate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
        if (params.id && typeof (params.id) == 'string') {

            this.gameHistoryService.getById(params.id).then(
                response => {
                    if (response.statusCode >= 200 && response.statusCode < 300) {
                        this._alert = null;
                        this.gameHistory = response.data!;
                    } else {
                        // show error message
                        this._alert = {
                            message: response.statusCode.toString() + ' - ' + response.errorMessage,
                            type: AlertType.Danger,
                            dismissable: true,
                        }
                    }
                }
            );
        }
    }

    async formSubmitted(event: Event) {
        await this.gameHistoryService
            .update(this.gameHistory!)
            .then(response => {
                    if (response.statusCode >= 200 && response.statusCode < 300) {
                        this._alert = null;
                        this.router.navigateToRoute('gameHistories-index', {});
                    } else {
                        // show error message
                        this._alert = {
                            message: response.statusCode.toString() + ' - ' + response.errorMessage,
                            type: AlertType.Danger,
                            dismissable: true,
                        }
                    }
                }
            );
        event.preventDefault();
    }

}
