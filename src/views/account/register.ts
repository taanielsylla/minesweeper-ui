import {autoinject} from 'aurelia-framework';
import {RouteConfig, NavigationInstruction, Router} from 'aurelia-router';
import {IAlertData} from 'types/IAlertData';
import {AccountService} from "../../services/account-service";
import {AppState} from "../../state/app-state";

@autoinject
export class AccountRegister {
    private _alert: IAlertData | null = null;

    private router: Router;
    private email?: string;
    private password?: string;
    private firstName?: string;
    private lastName?: string;
    private nickName?: string;

    private errorMessage: string | null = null

    constructor(private accountService: AccountService, router: Router, private appState: AppState,) {
        this.router = router;
    }

    async onSubmit(event: Event) {
        event.preventDefault();

        this.accountService.register({
            email: this.email,
            password: this.password,
            firstName: this.firstName,
            lastName: this.lastName,
            nickName: this.nickName,
        })
            .then(res => {
                console.log(res.statusCode);
                if (res.statusCode == 200) {
                    this.appState.JWT = res.data!.token;
                    this.router!.navigateToRoute('home-index');
                } else {
                    this.errorMessage = res.statusCode.toString() + ' ' + res.errorMessage!;
                }
            })
            .catch(err => {
                console.log("REGISTRATION ERROR: " + err.toString());
            })
    }
}
