import {Router} from 'aurelia-router';
import {autoinject} from 'aurelia-framework';
import {AccountService} from "../../services/account-service";
import {AppState} from "../../state/app-state";

@autoinject
export class AccountLogin {

    private email: string = ""
    private password: string = ""
    private errorMessage: string | null = null

    constructor(private accountService: AccountService, private appState: AppState, private router: Router) {
    }

    onSubmit(event: Event) {
        // console.log(this.email, this.password);
        event.preventDefault();

        this.accountService.login(this.email, this.password)
            .then(res => {
                console.log(res.statusCode);
                if (res.statusCode == 200) {
                    this.appState.JWT = res.data!.token;
                    this.router!.navigateToRoute('home-index');
                } else {
                    this.errorMessage = res.statusCode.toString() + ' ' + res.errorMessage!;
                }
            })
            .catch(err => {
                console.log("LOGIN ERROR: " + err.toString());
            })
    }
}
