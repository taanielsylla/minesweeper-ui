import {autoinject} from 'aurelia-framework';
import {RouteConfig, NavigationInstruction, Router} from 'aurelia-router';
import {IAlertData} from 'types/IAlertData';
import {AlertType} from 'types/AlertType';
import {TransactionService} from "../../services/transaction-service";

@autoinject
export class TransactionsCreate {
    private _alert: IAlertData | null = null;

    private appUserId?: string;
    private currencyTypeId?: string;
    private transactionStatusId?: string;
    private transactionTypeId?: string;
    private amount?: number;
    private endingBalance?: number;

    private router: Router;

    constructor(private transactionService: TransactionService, router: Router) {
        this.router = router;
    }

    async formSubmitted(event: Event) {
        await this.transactionService
            .create({
                appUserId: this.appUserId,
                currencyTypeId: this.currencyTypeId,
                transactionStatusId: this.transactionStatusId,
                transactionTypeId: this.transactionTypeId,
                amount: this.amount,
                endingBalance: this.endingBalance,
            })
            .then(response => {
                    if (response.statusCode >= 200 && response.statusCode < 300) {
                        this._alert = null;
                        this.router.navigateToRoute('transactions-index', {});
                    } else {
                        // show error message
                        this._alert = {
                            message: response.statusCode.toString() + ' - ' + response.errorMessage,
                            type: AlertType.Danger,
                            dismissable: true,
                        }
                    }
                }
            );

        event.preventDefault();
    }
}
