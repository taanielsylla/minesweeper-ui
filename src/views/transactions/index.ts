import {autoinject} from 'aurelia-framework';
import {AlertType} from '../../types/AlertType';
import {IAlertData} from 'types/IAlertData';
import {ITransaction} from "../../domain/ITransaction";
import {TransactionService} from "../../services/transaction-service";

@autoinject
export class TransactionsIndex {
    private _alert: IAlertData | null = null;

    private transactions: ITransaction[] = [];

    constructor(private transactionService: TransactionService) {

    }

    async attached() {
        await this.transactionService.getAll()

            .then(response => {
                    if (response.statusCode >= 200 && response.statusCode < 300) {
                        this._alert = null;
                        this.transactions = response.data;
                    } else {
                        // show error message
                        this._alert = {
                            message: response.statusCode.toString() + ' - ' + response.errorMessage,
                            type: AlertType.Danger,
                            dismissable: true,
                        }
                    }
                }
            );
    }

}
