import {autoinject} from 'aurelia-framework';
import {AlertType} from '../../types/AlertType';
import {IAlertData} from 'types/IAlertData';
import {ICurrencyType} from "../../domain/ICurrencyType";
import {CurrencyTypeService} from "../../services/currency-type-service";

@autoinject
export class CurrencyTypesIndex {
    private _alert: IAlertData | null = null;

    private currencyTypes: ICurrencyType[] = [];

    constructor(private currencyTypeService: CurrencyTypeService) {

    }

    async attached() {
        await this.currencyTypeService.getAll()

            .then(response => {
                    if (response.statusCode >= 200 && response.statusCode < 300) {
                        this._alert = null;
                        this.currencyTypes = response.data;
                    } else {
                        // show error message
                        this._alert = {
                            message: response.statusCode.toString() + ' - ' + response.errorMessage,
                            type: AlertType.Danger,
                            dismissable: true,
                        }
                    }
                }
            );
    }

}
