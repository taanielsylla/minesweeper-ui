import {autoinject} from 'aurelia-framework';
import {RouteConfig, NavigationInstruction, Router} from 'aurelia-router';
import {IAlertData} from 'types/IAlertData';
import {AlertType} from 'types/AlertType';
import {CurrencyTypeService} from "../../services/currency-type-service";

@autoinject
export class CurrencyTypesCreate {
    private _alert: IAlertData | null = null;

    private name?: string;

    private router: Router;

    constructor(private currencyTypeService: CurrencyTypeService, router: Router) {
        this.router = router;
    }

    async formSubmitted(event: Event) {
        await this.currencyTypeService
            .create({
                name: this.name,
            })
            .then(response => {
                    if (response.statusCode >= 200 && response.statusCode < 300) {
                        this._alert = null;
                        this.router.navigateToRoute('currencyTypes-index', {});
                    } else {
                        // show error message
                        this._alert = {
                            message: response.statusCode.toString() + ' - ' + response.errorMessage,
                            type: AlertType.Danger,
                            dismissable: true,
                        }
                    }
                }
            );

        event.preventDefault();
    }
}
