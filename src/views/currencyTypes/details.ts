import {autoinject} from 'aurelia-framework';
import {RouteConfig, NavigationInstruction} from 'aurelia-router';
import {IAlertData} from 'types/IAlertData';
import {AlertType} from 'types/AlertType';
import {ICurrencyType} from "../../domain/ICurrencyType";
import {CurrencyTypeService} from "../../services/currency-type-service";

@autoinject
export class CurrencyTypesDetails {
    private _alert: IAlertData | null = null;

    private currencyType?: ICurrencyType;

    constructor(private currencyTypeService: CurrencyTypeService) {

    }

    async activate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
        if (params.id && typeof (params.id) == 'string') {

            return await this.currencyTypeService.getById(params.id).then(
                response => {
                    if (response.statusCode >= 200 && response.statusCode < 300) {
                        this._alert = null;
                        this.currencyType = response.data!;
                    } else {
                        // show error message
                        this._alert = {
                            message: response.statusCode.toString() + ' - ' + response.errorMessage,
                            type: AlertType.Danger,
                            dismissable: true,
                        }
                        this.currencyType = undefined;
                    }
                }
            );
        }
    }

}
