import {autoinject} from 'aurelia-framework';
import {RouteConfig, NavigationInstruction} from 'aurelia-router';
import {IAlertData} from 'types/IAlertData';
import {AlertType} from 'types/AlertType';
import {AffiliateService} from "../../services/affiliate-service";
import {IAffiliate} from "../../domain/IAffiliate";

@autoinject
export class TransactionTypesDetails {
    private _alert: IAlertData | null = null;

    private affiliate?: IAffiliate;

    constructor(private affiliateService: AffiliateService) {

    }

    async activate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
        if (params.id && typeof (params.id) == 'string') {

            return await this.affiliateService.getById(params.id).then(
                response => {
                    if (response.statusCode >= 200 && response.statusCode < 300) {
                        this._alert = null;
                        this.affiliate = response.data!;
                    } else {
                        // show error message
                        this._alert = {
                            message: response.statusCode.toString() + ' - ' + response.errorMessage,
                            type: AlertType.Danger,
                            dismissable: true,
                        }
                        this.affiliate = undefined;
                    }
                }
            );
        }
    }

}
