import {autoinject} from 'aurelia-framework';
import {RouteConfig, NavigationInstruction, Router} from 'aurelia-router';
import {IAlertData} from 'types/IAlertData';
import {AlertType} from 'types/AlertType';
import {AffiliateService} from "../../services/affiliate-service";
import {IAffiliate} from "../../domain/IAffiliate";


@autoinject
export class TransactionTypesDelete {
    private _alert: IAlertData | null = null;

    private router: Router;
    private affiliate?: IAffiliate;

    constructor(private affiliateService: AffiliateService, router: Router) {
        this.router = router;
    }

    async activate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
        if (params.id && typeof (params.id) == 'string') {

            await this.affiliateService.getById(params.id)
                .then(response => {
                    if (response.statusCode >= 200 && response.statusCode < 300) {
                        this._alert = null;
                        this.affiliate = response.data!;
                    } else {
                        // show error message
                        this._alert = {
                            message: response.statusCode.toString() + ' - ' + response.errorMessage,
                            type: AlertType.Danger,
                            dismissable: true,
                        };
                        this.affiliate = undefined;
                    }
                });
        }
    }

    async formSubmitted(event: Event) {
        await this.affiliateService
            .delete(this.affiliate!.id)
            .then(response => {
                if (response.statusCode >= 200 && response.statusCode < 300) {
                    this._alert = null;
                    this.router.navigateToRoute('affiliates-index', {});
                } else {
                    // show error message
                    this._alert = {
                        message: response.statusCode.toString() + ' - ' + response.errorMessage,
                        type: AlertType.Danger,
                        dismissable: true,
                    }
                }
            });
        event.preventDefault();
    }
}
