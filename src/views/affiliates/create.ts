import {autoinject} from 'aurelia-framework';
import {RouteConfig, NavigationInstruction, Router} from 'aurelia-router';
import {IAlertData} from 'types/IAlertData';
import {AlertType} from 'types/AlertType';
import {AffiliateService} from "../../services/affiliate-service";

@autoinject
export class AffiliatesCreate {
    private _alert: IAlertData | null = null;

    private appUserId?: string;
    private referred?: string;

    private router: Router;

    constructor(private affiliateService: AffiliateService, router: Router) {
        this.router = router;
    }

    async formSubmitted(event: Event) {
        await this.affiliateService
            .create({
                appUserId: this.appUserId,
                referredTo: this.referred,
            })
            .then(response => {
                    if (response.statusCode >= 200 && response.statusCode < 300) {
                        this._alert = null;
                        this.router.navigateToRoute('affiliates-index', {});
                    } else {
                        // show error message
                        this._alert = {
                            message: response.statusCode.toString() + ' - ' + response.errorMessage,
                            type: AlertType.Danger,
                            dismissable: true,
                        }
                    }
                }
            );

        event.preventDefault();
    }
}
