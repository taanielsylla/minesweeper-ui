import {autoinject} from 'aurelia-framework';
import {AlertType} from '../../types/AlertType';
import {IAlertData} from 'types/IAlertData';
import {IAffiliate} from "../../domain/IAffiliate";
import {AffiliateService} from "../../services/affiliate-service";

@autoinject
export class AffiliatesIndex {
    private _alert: IAlertData | null = null;

    private affiliates: IAffiliate[] = [];

    constructor(private affiliateService: AffiliateService) {

    }

    async attached() {
        await this.affiliateService.getAll()

            .then(response => {
                    if (response.statusCode >= 200 && response.statusCode < 300) {
                        this._alert = null;
                        this.affiliates = response.data;
                    } else {
                        // show error message
                        this._alert = {
                            message: response.statusCode.toString() + ' - ' + response.errorMessage,
                            type: AlertType.Danger,
                            dismissable: true,
                        }
                    }
                }
            );
    }

}
