import {autoinject} from 'aurelia-framework';
import {AlertType} from '../../types/AlertType';
import {IAlertData} from 'types/IAlertData';
import {IGameLevel} from "../../domain/IGameLevel";
import {GameLevelService} from "../../services/game-level-service";

@autoinject
export class GameLevelsIndex {
    private _alert: IAlertData | null = null;

    private gameLevels: IGameLevel[] = [];

    constructor(private gameLevelService: GameLevelService) {

    }

    async attached() {
        await this.gameLevelService.getAll()

            .then(response => {
                    if (response.statusCode >= 200 && response.statusCode < 300) {
                        this._alert = null;
                        this.gameLevels = response.data;
                    } else {
                        // show error message
                        this._alert = {
                            message: response.statusCode.toString() + ' - ' + response.errorMessage,
                            type: AlertType.Danger,
                            dismissable: true,
                        }
                    }
                }
            );
    }

}
