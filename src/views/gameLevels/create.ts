import {autoinject} from 'aurelia-framework';
import {RouteConfig, NavigationInstruction, Router} from 'aurelia-router';
import {IAlertData} from 'types/IAlertData';
import {AlertType} from 'types/AlertType';
import {GameLevelService} from "../../services/game-level-service";

@autoinject
export class GameLevelsCreate {
    private _alert: IAlertData | null = null;

    private gameModeId?: string;
    private bet?: number;
    private levelNr?: number;
    private boxCount?: number;

    private router: Router;

    constructor(private gameLevelService: GameLevelService, router: Router) {
        this.router = router;
    }

    async formSubmitted(event: Event) {
        await this.gameLevelService
            .create({
                gameModeId: this.gameModeId,
                bet: this.bet,
                levelNr: this.levelNr,
                boxCount: this.boxCount,
            })
            .then(response => {
                    if (response.statusCode >= 200 && response.statusCode < 300) {
                        this._alert = null;
                        this.router.navigateToRoute('gameLevels-index', {});
                    } else {
                        // show error message
                        this._alert = {
                            message: response.statusCode.toString() + ' - ' + response.errorMessage,
                            type: AlertType.Danger,
                            dismissable: true,
                        }
                    }
                }
            );

        event.preventDefault();
    }
}
