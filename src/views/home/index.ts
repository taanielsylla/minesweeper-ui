import {autoinject} from "aurelia-framework";
import {IAlertData} from "../../types/IAlertData";
import {GameService} from "../../services/game-service";
import {IGameDataView} from "../../domain/view/IGameDataView";
import {IFetchResponse} from "../../types/IFetchResponse";
import {IConfigurationView} from "../../domain/view/IConfigurationView";
import {CurrencyManager} from "../../components/CurrencyManager";
import {GameBoard} from "../../components/GameBoard";
import {AppState, ENUM} from "../../state/app-state";
import {AlertType} from "../../types/AlertType";
import {IStateView} from "../../domain/view/IStateView";
import {IWalletView} from "../../domain/view/IWalletView";

const enums = {
    NO_GAME: "NO_GAME",
    START: "START",
    TAKE: "TAKE",
    BALANCE: "Balance: ",
}

@autoinject
export class HomeIndex {

    private _alert: IAlertData | null = null;
    private gameData: IFetchResponse<IGameDataView>;

    availableGameModes: string[];
    availableCurrencies: string[];
    selectedCurrency?: string;
    selectedGameMode?: string;
    selectedConfiguration?: IConfigurationView;

    gameButtonValue = enums.START;

    private board: GameBoard;
    private gridContainer: HTMLDivElement;
    private difficultyRadioBtns: HTMLDivElement;
    private currencyTypeSelector: HTMLDivElement;
    private balanceLabel: HTMLSpanElement;
    private nickNameLabel: HTMLSpanElement;

    constructor(private gameService: GameService, private state: AppState) {

    }

    async activate() {
        await this.parseData();
    }

    async attached() {
        document.body.classList.add("gameBg") // add jungle bg
        this.gridContainer = document.getElementById("gridContainer") as HTMLDivElement;
        this.difficultyRadioBtns = document.getElementById("difficultyRadio") as HTMLDivElement;
        this.currencyTypeSelector = document.getElementById("currencyTypeSelector") as HTMLDivElement;
        this.balanceLabel = document.getElementById("balance") as HTMLSpanElement;
        this.nickNameLabel = document.getElementById("nickName") as HTMLSpanElement;
        this.redrawBoard();

        const activeState = await this.gameService.getState();
        if (activeState.statusCode === 200) { // found active game
            this.onResume(activeState.data);
        }

        if (!this.state.isLoggedIn()) { // do not show nickname if user not logged in
            this.nickNameLabel.parentElement.removeChild(this.nickNameLabel);
        }

        document.getElementById('mainWrapper').classList.remove("loader"); // remove loader spinner
    }

    detached() {
        document.body.classList.remove("gameBg") // remove jungle bg
    }

    private onResume(data: IStateView) {
        this.state.parseUserState(data);
        this.updateBalance();
        if (data.status !== enums.NO_GAME) {
            this.gameButtonValue = enums.TAKE;
            this.board.openToRow(this.state.currentLevel);
        }
        this.updateNickName();
    }

    async onCellClick(cell, r, c, i) {
        const selectedWallet: IWalletView = this.state.wallets?.filter(w => w.currencyName === this.state.selectedCurrency)[0];
        const balance = selectedWallet ? selectedWallet.amount : 0;
        const bet = parseFloat(cell.dataset.bet);
        if ((balance - bet) < 0) {
            alert("Insufficient funds for a bet of " + bet.toFixed(2) + " " + this.state.selectedCurrency);
            return;
        }

        const betData = await this.gameService.bet();
        const success = betData.statusCode >= 200 && betData.statusCode < 300;
        if (!success) {
            alert("BET action failed with status code: " + betData.statusCode + ", message: " + betData.errorMessage);
            return;
        }

        this.state.parseUserState(betData.data);
        this.updateBalance();
        this.updateNickName();

        if (this.state.win === 0) { // LOSE, resolved round
            cell.style.background = "#e90000"
            this.gameButtonValue = enums.START;

        } else { // WIN
            cell.style.background = "#59e557"
            cell.closest("tr").classList.add("opened");
            await new Promise(resolve => setTimeout(resolve, 2500)); // wait for open animation to finish
            if (this.state.roundStatus === ENUM.ACTIVE) this.board.openToRow(this.state.currentLevel);
            else {
                this.gameButtonValue = enums.START;
            }
        }
    }

    async onGameButtonSelect() {
        if (this.gameButtonValue === enums.START) {
            const newState = await this.gameService.init({configurationId: this.state.selectedConfiguration.id})
                .then(response => {
                        if (response.statusCode >= 200 && response.statusCode < 300) {
                            this._alert = null;
                            this.onInit(response.data);
                        } else {
                            // show error message
                            const err = response.errorMessage;
                            const msg = err.includes('Unauthorized') ? err + ". Please log in!" : err;
                            this._alert = {
                                message: response.statusCode.toString() + ' - ' + msg,
                                type: AlertType.Danger,
                                dismissable: true,
                            }
                        }
                    }
                );
        } else if (this.gameButtonValue === enums.TAKE) {
            // take win, resolve game
            await this.resolveGame();
        }
    }

    async parseData() {
        this.gameData = await this.gameService.getGameData();
        if (!this.gameData) {
            return;
        }

        this.state.gameData = this.gameData.data;
        this.availableGameModes = this.gameData.data.gameModes;
        this.availableCurrencies = this.gameData.data.currencies;

        this.selectedGameMode = this.state.selectedGameMode;
        if (!this.selectedGameMode) {
            this.selectedGameMode = this.gameData.data.configurations[0].gameMode; // Easy
            this.state.selectedGameMode = this.selectedGameMode;
        }

        this.selectedCurrency = this.state.selectedCurrency;
        if (!this.selectedCurrency) {
            this.selectedCurrency = this.gameData.data.currencies[1]; // mBTC
            this.state.selectedCurrency = this.selectedCurrency;
        }
        CurrencyManager.updateSelectedCurrency(this.selectedCurrency);

        this.selectedConfiguration = this.state.selectedConfiguration;
        if (!this.selectedConfiguration) { // if not defined, get configuration of selected currency & selected game mode
            this.selectedConfiguration = this.gameData.data.configurations.filter(d => d.gameMode === this.selectedGameMode && d.currencyType === this.selectedCurrency)[0];
            this.state.selectedConfiguration = this.selectedConfiguration;
        }
    }

    onCurrencySelect(currencyName: string) {
        this.selectedCurrency = currencyName;
        CurrencyManager.updateSelectedCurrency(this.selectedCurrency)
        this.state.selectedCurrency = this.selectedCurrency;
        this.state.selectedConfiguration = this.selectedConfiguration = this.gameData.data.configurations.filter(d => {
            return d.gameMode === this.selectedGameMode && d.currencyType === this.selectedCurrency
        })[0];
        this.redrawBoard();
    }

    private redrawBoard() {
        while (this.gridContainer.firstChild) {
            this.gridContainer.removeChild(this.gridContainer.lastChild);
        }
        this.board = new GameBoard(this.gridContainer, this.selectedConfiguration, this.onCellClick.bind(this));
        this.updateBalance();
    }

    onGameModeSelect(gameMode: string) {
        this.state.selectedGameMode = this.selectedGameMode = gameMode;
        this.state.selectedConfiguration = this.selectedConfiguration = this.gameData.data.configurations.filter(d => {
            return d.gameMode === this.selectedGameMode && d.currencyType === this.selectedCurrency
        })[0];
        this.redrawBoard();
    }

    private onInit(data: IStateView) {
        this.state.parseUserState(data);
        this.redrawBoard();
        this.board.openToRow(this.state.currentLevel);
        this.gameButtonValue = enums.TAKE;
        this.updateNickName();
    }

    private async resolveGame() {
        const resolveData = await this.gameService.resolve();
        const success = resolveData.statusCode >= 200 && resolveData.statusCode < 300;
        if (!success) {
            alert("RESOLVE action failed with status code: " + resolveData.statusCode + ", message: " + resolveData.errorMessage);
            return;
        }
        this.state.parseUserState(resolveData.data);
        this.redrawBoard();
        this.gameButtonValue = enums.START;
        this.updateNickName();
    }

    updateBalance() {
        // get selected wallet view and show balance for that currency
        const selectedWallet: IWalletView = this.state.wallets?.filter(w => w.currencyName === this.state.selectedCurrency)[0];
        let amount = selectedWallet ? selectedWallet.amount.toFixed(2) : 0;

        this.balanceLabel.innerHTML = enums.BALANCE + amount;
    }

    updateNickName() {
        this.nickNameLabel.innerHTML = this.state.nickName;
    }
}
