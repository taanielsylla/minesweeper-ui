import {autoinject} from 'aurelia-framework';
import {RouteConfig, NavigationInstruction, Router} from 'aurelia-router';
import {IAlertData} from 'types/IAlertData';
import {AlertType} from 'types/AlertType';
import {TransactionTypeService} from "../../services/transaction-type-service";

@autoinject
export class TransactionTypesCreate {
    private _alert: IAlertData | null = null;

    private name?: string;

    private router: Router;

    constructor(private transactionTypeService: TransactionTypeService, router: Router) {
        this.router = router;
    }

    async formSubmitted(event: Event) {
        await this.transactionTypeService
            .create({
                name: this.name,
            })
            .then(response => {
                    if (response.statusCode >= 200 && response.statusCode < 300) {
                        this._alert = null;
                        this.router.navigateToRoute('transactionTypes-index', {});
                    } else {
                        // show error message
                        this._alert = {
                            message: response.statusCode.toString() + ' - ' + response.errorMessage,
                            type: AlertType.Danger,
                            dismissable: true,
                        }
                    }
                }
            );

        event.preventDefault();
    }
}
