import {autoinject} from 'aurelia-framework';
import {AlertType} from '../../types/AlertType';
import {IAlertData} from 'types/IAlertData';
import {ITransactionType} from "../../domain/ITransactionType";
import {TransactionTypeService} from "../../services/transaction-type-service";

@autoinject
export class TransactionTypesIndex {
    private _alert: IAlertData | null = null;

    private transactionTypes: ITransactionType[] = [];

    constructor(private transactionTypeService: TransactionTypeService) {

    }

    async attached() {
        await this.transactionTypeService.getAll()

            .then(response => {
                    if (response.statusCode >= 200 && response.statusCode < 300) {
                        this._alert = null;
                        this.transactionTypes = response.data;
                    } else {
                        // show error message
                        this._alert = {
                            message: response.statusCode.toString() + ' - ' + response.errorMessage,
                            type: AlertType.Danger,
                            dismissable: true,
                        }
                    }
                }
            );
    }

}
