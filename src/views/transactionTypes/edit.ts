import {autoinject} from 'aurelia-framework';
import {RouteConfig, NavigationInstruction, Router} from 'aurelia-router';
import {IAlertData} from 'types/IAlertData';
import {AlertType} from 'types/AlertType';
import {ITransactionType} from "../../domain/ITransactionType";
import {TransactionTypeService} from "../../services/transaction-type-service";

@autoinject
export class TransactionTypesEdit {
    private _alert: IAlertData | null = null;

    router?: Router;
    private transactionType?: ITransactionType;

    constructor(private transactionTypeService: TransactionTypeService, router: Router) {
        this.router = router;
    }

    // can bind params in this lifecycle hook
    activate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
        if (params.id && typeof (params.id) == 'string') {

            this.transactionTypeService.getById(params.id).then(
                response => {
                    if (response.statusCode >= 200 && response.statusCode < 300) {
                        this._alert = null;
                        this.transactionType = response.data!;
                    } else {
                        // show error message
                        this._alert = {
                            message: response.statusCode.toString() + ' - ' + response.errorMessage,
                            type: AlertType.Danger,
                            dismissable: true,
                        }
                    }
                }
            );
        }
    }

    async formSubmitted(event: Event) {
        await this.transactionTypeService
            .update(this.transactionType!)
            .then(response => {
                    if (response.statusCode >= 200 && response.statusCode < 300) {
                        this._alert = null;
                        this.router.navigateToRoute('transactionTypes-index', {});
                    } else {
                        // show error message
                        this._alert = {
                            message: response.statusCode.toString() + ' - ' + response.errorMessage,
                            type: AlertType.Danger,
                            dismissable: true,
                        }
                    }
                }
            );
        event.preventDefault();
    }

}
