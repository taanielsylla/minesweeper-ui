import {autoinject} from 'aurelia-framework';
import {RouteConfig, NavigationInstruction} from 'aurelia-router';
import {IAlertData} from 'types/IAlertData';
import {AlertType} from 'types/AlertType';
import {ITransactionType} from "../../domain/ITransactionType";
import {TransactionTypeService} from "../../services/transaction-type-service";

@autoinject
export class TransactionTypesDetails {
    private _alert: IAlertData | null = null;

    private transactionType?: ITransactionType;

    constructor(private transactionTypeService: TransactionTypeService) {

    }

    async activate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
        if (params.id && typeof (params.id) == 'string') {

            return await this.transactionTypeService.getById(params.id).then(
                response => {
                    if (response.statusCode >= 200 && response.statusCode < 300) {
                        this._alert = null;
                        this.transactionType = response.data!;
                    } else {
                        // show error message
                        this._alert = {
                            message: response.statusCode.toString() + ' - ' + response.errorMessage,
                            type: AlertType.Danger,
                            dismissable: true,
                        }
                        this.transactionType = undefined;
                    }
                }
            );
        }
    }

}
