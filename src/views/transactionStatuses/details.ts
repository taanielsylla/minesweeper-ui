import {autoinject} from 'aurelia-framework';
import {RouteConfig, NavigationInstruction} from 'aurelia-router';
import {IAlertData} from 'types/IAlertData';
import {AlertType} from 'types/AlertType';
import {TransactionStatusService} from "../../services/transaction-status-service";
import {ITransactionStatus} from "../../domain/ITransactionStatus";

@autoinject
export class TransactionStatusesDetails {
    private _alert: IAlertData | null = null;

    private transactionStatus?: ITransactionStatus;

    constructor(private transactionStatusService: TransactionStatusService) {

    }

    async activate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
        if (params.id && typeof (params.id) == 'string') {

            return await this.transactionStatusService.getById(params.id).then(
                response => {
                    if (response.statusCode >= 200 && response.statusCode < 300) {
                        this._alert = null;
                        this.transactionStatus = response.data!;
                    } else {
                        // show error message
                        this._alert = {
                            message: response.statusCode.toString() + ' - ' + response.errorMessage,
                            type: AlertType.Danger,
                            dismissable: true,
                        }
                        this.transactionStatus = undefined;
                    }
                }
            );
        }
    }

}
