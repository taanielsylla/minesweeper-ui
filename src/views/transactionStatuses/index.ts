import {autoinject} from 'aurelia-framework';
import {AlertType} from '../../types/AlertType';
import {IAlertData} from 'types/IAlertData';
import {ITransactionStatus} from "../../domain/ITransactionStatus";
import {TransactionStatusService} from "../../services/transaction-status-service";

@autoinject
export class TransactionStatusesIndex {
    private _alert: IAlertData | null = null;

    private transactionStatuses: ITransactionStatus[] = [];

    constructor(private transactionStatusService: TransactionStatusService) {

    }

    async attached() {
        await this.transactionStatusService.getAll()

            .then(response => {
                    if (response.statusCode >= 200 && response.statusCode < 300) {
                        this._alert = null;
                        this.transactionStatuses = response.data;
                    } else {
                        // show error message
                        this._alert = {
                            message: response.statusCode.toString() + ' - ' + response.errorMessage,
                            type: AlertType.Danger,
                            dismissable: true,
                        }
                    }
                }
            );
    }

}
