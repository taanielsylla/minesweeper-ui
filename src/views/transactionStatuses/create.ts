import {autoinject} from 'aurelia-framework';
import {RouteConfig, NavigationInstruction, Router} from 'aurelia-router';
import {IAlertData} from 'types/IAlertData';
import {AlertType} from 'types/AlertType';
import {TransactionStatusService} from "../../services/transaction-status-service";

@autoinject
export class TransactionStatusesCreate {
    private _alert: IAlertData | null = null;

    private name?: string;

    private router: Router;

    constructor(private transactionStatusService: TransactionStatusService, router: Router) {
        this.router = router;
    }

    async formSubmitted(event: Event) {
        await this.transactionStatusService
            .create({
                name: this.name,
            })
            .then(response => {
                    if (response.statusCode >= 200 && response.statusCode < 300) {
                        this._alert = null;
                        this.router.navigateToRoute('transactionStatuses-index', {});
                    } else {
                        // show error message
                        this._alert = {
                            message: response.statusCode.toString() + ' - ' + response.errorMessage,
                            type: AlertType.Danger,
                            dismissable: true,
                        }
                    }
                }
            );

        event.preventDefault();
    }
}
