import {autoinject} from 'aurelia-framework';
import {RouteConfig, NavigationInstruction, Router} from 'aurelia-router';
import {IAlertData} from 'types/IAlertData';
import {AlertType} from 'types/AlertType';
import {TransactionStatusService} from "../../services/transaction-status-service";
import {ITransactionStatus} from "../../domain/ITransactionStatus";

@autoinject
export class TransactionStatusesEdit {
    private _alert: IAlertData | null = null;

    router?: Router;
    private transactionStatus?: ITransactionStatus;

    constructor(private transactionStatusService: TransactionStatusService, router: Router) {
        this.router = router;
    }

    // can bind params in this lifecycle hook
    activate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
        if (params.id && typeof (params.id) == 'string') {

            this.transactionStatusService.getById(params.id).then(
                response => {
                    if (response.statusCode >= 200 && response.statusCode < 300) {
                        this._alert = null;
                        this.transactionStatus = response.data!;
                    } else {
                        // show error message
                        this._alert = {
                            message: response.statusCode.toString() + ' - ' + response.errorMessage,
                            type: AlertType.Danger,
                            dismissable: true,
                        }
                    }
                }
            );
        }
    }

    async formSubmitted(event: Event) {
        await this.transactionStatusService
            .update(this.transactionStatus!)
            .then(response => {
                    if (response.statusCode >= 200 && response.statusCode < 300) {
                        this._alert = null;
                        this.router.navigateToRoute('transactionStatuses-index', {});
                    } else {
                        // show error message
                        this._alert = {
                            message: response.statusCode.toString() + ' - ' + response.errorMessage,
                            type: AlertType.Danger,
                            dismissable: true,
                        }
                    }
                }
            );
        event.preventDefault();
    }

}
