import {autoinject} from 'aurelia-framework';
import {RouteConfig, NavigationInstruction, Router} from 'aurelia-router';
import {IAlertData} from 'types/IAlertData';
import {AlertType} from 'types/AlertType';
import {ConfigurationService} from "../../services/configuration-service";

@autoinject
export class CurrencyTypesCreate {
    private _alert: IAlertData | null = null;

    private currencyTypeId?: string;
    private name?: string;
    private gameModeId?: string;

    private router: Router;

    constructor(private configurationService: ConfigurationService, router: Router) {
        this.router = router;
    }

    async formSubmitted(event: Event) {
        await this.configurationService
            .create({
                currencyTypeId: this.currencyTypeId,
                name: this.name,
                gameModeId: this.gameModeId,
            })
            .then(response => {
                    if (response.statusCode >= 200 && response.statusCode < 300) {
                        this._alert = null;
                        this.router.navigateToRoute('configurations-index', {});
                    } else {
                        // show error message
                        this._alert = {
                            message: response.statusCode.toString() + ' - ' + response.errorMessage,
                            type: AlertType.Danger,
                            dismissable: true,
                        }
                    }
                }
            );

        event.preventDefault();
    }
}
