import {autoinject} from 'aurelia-framework';
import {AlertType} from '../../types/AlertType';
import {IAlertData} from 'types/IAlertData';
import {IConfiguration} from "../../domain/IConfiguration";
import {ConfigurationService} from "../../services/configuration-service";

@autoinject
export class ConfigurationsIndex {
    private _alert: IAlertData | null = null;

    private configurations: IConfiguration[] = [];

    constructor(private configurationService: ConfigurationService) {

    }

    async attached() {
        await this.configurationService.getAll()

            .then(response => {
                    if (response.statusCode >= 200 && response.statusCode < 300) {
                        this._alert = null;
                        this.configurations = response.data;
                    } else {
                        // show error message
                        this._alert = {
                            message: response.statusCode.toString() + ' - ' + response.errorMessage,
                            type: AlertType.Danger,
                            dismissable: true,
                        }
                    }
                }
            );
    }

}
