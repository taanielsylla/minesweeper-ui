import {autoinject} from 'aurelia-framework';
import {RouteConfig, NavigationInstruction} from 'aurelia-router';
import {IAlertData} from 'types/IAlertData';
import {AlertType} from 'types/AlertType';
import {IFaucet} from "../../domain/IFaucet";
import {FaucetService} from "../../services/faucet-service";

@autoinject
export class FaucetsDetails {
    private _alert: IAlertData | null = null;

    private faucet?: IFaucet;

    constructor(private faucetService: FaucetService) {

    }

    async activate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
        if (params.id && typeof (params.id) == 'string') {

            return await this.faucetService.getById(params.id).then(
                response => {
                    if (response.statusCode >= 200 && response.statusCode < 300) {
                        this._alert = null;
                        this.faucet = response.data!;
                    } else {
                        // show error message
                        this._alert = {
                            message: response.statusCode.toString() + ' - ' + response.errorMessage,
                            type: AlertType.Danger,
                            dismissable: true,
                        }
                        this.faucet = undefined;
                    }
                }
            );
        }
    }

}
