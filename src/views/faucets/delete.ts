import {autoinject} from 'aurelia-framework';
import {RouteConfig, NavigationInstruction, Router} from 'aurelia-router';
import {IAlertData} from 'types/IAlertData';
import {AlertType} from 'types/AlertType';
import {IFaucet} from "../../domain/IFaucet";
import {FaucetService} from "../../services/faucet-service";


@autoinject
export class FaucetsDelete {
    private _alert: IAlertData | null = null;

    private router: Router;
    private faucet?: IFaucet;

    constructor(private faucetService: FaucetService, router: Router) {
        this.router = router;
    }

    async activate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
        if (params.id && typeof (params.id) == 'string') {

            await this.faucetService.getById(params.id)
                .then(response => {
                    if (response.statusCode >= 200 && response.statusCode < 300) {
                        this._alert = null;
                        this.faucet = response.data!;
                    } else {
                        // show error message
                        this._alert = {
                            message: response.statusCode.toString() + ' - ' + response.errorMessage,
                            type: AlertType.Danger,
                            dismissable: true,
                        };
                        this.faucet = undefined;
                    }
                });
        }
    }

    async formSubmitted(event: Event) {
        await this.faucetService
            .delete(this.faucet!.id)
            .then(response => {
                if (response.statusCode >= 200 && response.statusCode < 300) {
                    this._alert = null;
                    this.router.navigateToRoute('faucets-index', {});
                } else {
                    // show error message
                    this._alert = {
                        message: response.statusCode.toString() + ' - ' + response.errorMessage,
                        type: AlertType.Danger,
                        dismissable: true,
                    }
                }
            });
        event.preventDefault();
    }
}
