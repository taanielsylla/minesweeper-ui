import {autoinject} from 'aurelia-framework';
import {AlertType} from '../../types/AlertType';
import {IAlertData} from 'types/IAlertData';
import {IFaucet} from "../../domain/IFaucet";
import {FaucetService} from "../../services/faucet-service";

@autoinject
export class FaucetsIndex {
    private _alert: IAlertData | null = null;

    private faucets: IFaucet[] = [];

    constructor(private faucetService: FaucetService) {

    }

    async attached() {
        await this.faucetService.getAll()

            .then(response => {
                    if (response.statusCode >= 200 && response.statusCode < 300) {
                        this._alert = null;
                        this.faucets = response.data;
                    } else {
                        // show error message
                        this._alert = {
                            message: response.statusCode.toString() + ' - ' + response.errorMessage,
                            type: AlertType.Danger,
                            dismissable: true,
                        }
                    }
                }
            );
    }

}
