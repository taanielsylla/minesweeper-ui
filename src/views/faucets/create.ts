import {autoinject} from 'aurelia-framework';
import {RouteConfig, NavigationInstruction, Router} from 'aurelia-router';
import {IAlertData} from 'types/IAlertData';
import {AlertType} from 'types/AlertType';
import {FaucetService} from "../../services/faucet-service";

@autoinject
export class FaucetsCreate {
    private _alert: IAlertData | null = null;

    private appUserId?: string;
    private currencyTypeId?: string;
    private amountRetrieved?: number;

    private router: Router;

    constructor(private faucetService: FaucetService, router: Router) {
        this.router = router;
    }

    async formSubmitted(event: Event) {
        await this.faucetService
            .create({
                appUserId: this.appUserId,
                currencyTypeId: this.currencyTypeId,
                amountRetrieved: this.amountRetrieved,
            })
            .then(response => {
                    if (response.statusCode >= 200 && response.statusCode < 300) {
                        this._alert = null;
                        this.router.navigateToRoute('faucets-index', {});
                    } else {
                        // show error message
                        this._alert = {
                            message: response.statusCode.toString() + ' - ' + response.errorMessage,
                            type: AlertType.Danger,
                            dismissable: true,
                        }
                    }
                }
            );

        event.preventDefault();
    }
}
