import {autoinject} from 'aurelia-framework';
import {RouteConfig, NavigationInstruction, Router} from 'aurelia-router';
import {IAlertData} from 'types/IAlertData';
import {AlertType} from 'types/AlertType';
import {IWallet} from "../../domain/IWallet";
import {WalletService} from "../../services/wallet-service";


@autoinject
export class WalletssDelete {
    private _alert: IAlertData | null = null;

    private router: Router;
    private wallet?: IWallet;

    constructor(private walletService: WalletService, router: Router) {
        this.router = router;
    }

    async activate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
        if (params.id && typeof (params.id) == 'string') {

            await this.walletService.getById(params.id)
                .then(response => {
                    if (response.statusCode >= 200 && response.statusCode < 300) {
                        this._alert = null;
                        this.wallet = response.data!;
                    } else {
                        // show error message
                        this._alert = {
                            message: response.statusCode.toString() + ' - ' + response.errorMessage,
                            type: AlertType.Danger,
                            dismissable: true,
                        };
                        this.wallet = undefined;
                    }
                });
        }
    }

    async formSubmitted(event: Event) {
        await this.walletService
            .delete(this.wallet!.id)
            .then(response => {
                if (response.statusCode >= 200 && response.statusCode < 300) {
                    this._alert = null;
                    this.router.navigateToRoute('wallets-index', {});
                } else {
                    // show error message
                    this._alert = {
                        message: response.statusCode.toString() + ' - ' + response.errorMessage,
                        type: AlertType.Danger,
                        dismissable: true,
                    }
                }
            });
        event.preventDefault();
    }
}
