import {autoinject} from 'aurelia-framework';
import {RouteConfig, NavigationInstruction, Router} from 'aurelia-router';
import {IAlertData} from 'types/IAlertData';
import {AlertType} from 'types/AlertType';
import {WalletService} from "../../services/wallet-service";

@autoinject
export class WalletsCreate {
    private _alert: IAlertData | null = null;

    private appUserId?: string;
    private currencyTypeId?: string;
    private amount?: number;

    private router: Router;

    constructor(private walletService: WalletService, router: Router) {
        this.router = router;
    }

    async formSubmitted(event: Event) {
        await this.walletService
            .create({
                appUserId: this.appUserId,
                currencyTypeId: this.currencyTypeId,
                amount: this.amount,
            })
            .then(response => {
                    if (response.statusCode >= 200 && response.statusCode < 300) {
                        this._alert = null;
                        this.router.navigateToRoute('wallets-index', {});
                    } else {
                        // show error message
                        this._alert = {
                            message: response.statusCode.toString() + ' - ' + response.errorMessage,
                            type: AlertType.Danger,
                            dismissable: true,
                        }
                    }
                }
            );

        event.preventDefault();
    }
}
