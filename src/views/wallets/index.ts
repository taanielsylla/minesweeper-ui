import {autoinject} from 'aurelia-framework';
import {AlertType} from '../../types/AlertType';
import {IAlertData} from 'types/IAlertData';
import {IWallet} from "../../domain/IWallet";
import {WalletService} from "../../services/wallet-service";

@autoinject
export class WalletsIndex {
    private _alert: IAlertData | null = null;

    private wallets: IWallet[] = [];

    constructor(private walletService: WalletService) {

    }

    async attached() {
        await this.walletService.getAll()

            .then(response => {
                    if (response.statusCode >= 200 && response.statusCode < 300) {
                        this._alert = null;
                        this.wallets = response.data;
                    } else {
                        // show error message
                        this._alert = {
                            message: response.statusCode.toString() + ' - ' + response.errorMessage,
                            type: AlertType.Danger,
                            dismissable: true,
                        }
                    }
                }
            );
    }

}
