import {autoinject} from 'aurelia-framework';
import {AlertType} from '../../types/AlertType';
import {IAlertData} from 'types/IAlertData';
import {IRoundStatus} from "../../domain/IRoundStatus";
import {RoundStatusService} from "../../services/round-status-service";

@autoinject
export class RoundStatusesIndex {
    private _alert: IAlertData | null = null;

    private roundStatuses: IRoundStatus[] = [];

    constructor(private roundStatusService: RoundStatusService) {

    }

    async attached() {
        await this.roundStatusService.getAll()

            .then(response => {
                    if (response.statusCode >= 200 && response.statusCode < 300) {
                        this._alert = null;
                        this.roundStatuses = response.data;
                    } else {
                        // show error message
                        this._alert = {
                            message: response.statusCode.toString() + ' - ' + response.errorMessage,
                            type: AlertType.Danger,
                            dismissable: true,
                        }
                    }
                }
            );
    }

}
