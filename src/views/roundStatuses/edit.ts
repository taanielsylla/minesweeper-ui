import {autoinject} from 'aurelia-framework';
import {RouteConfig, NavigationInstruction, Router} from 'aurelia-router';
import {IAlertData} from 'types/IAlertData';
import {AlertType} from 'types/AlertType';
import {IRoundStatus} from "../../domain/IRoundStatus";
import {RoundStatusService} from "../../services/round-status-service";

@autoinject
export class RoundStatusesEdit {
    private _alert: IAlertData | null = null;

    router?: Router;
    private roundStatus?: IRoundStatus;

    constructor(private roundStatusService: RoundStatusService, router: Router) {
        this.router = router;
    }

    // can bind params in this lifecycle hook
    activate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
        if (params.id && typeof (params.id) == 'string') {

            this.roundStatusService.getById(params.id).then(
                response => {
                    if (response.statusCode >= 200 && response.statusCode < 300) {
                        this._alert = null;
                        this.roundStatus = response.data!;
                    } else {
                        // show error message
                        this._alert = {
                            message: response.statusCode.toString() + ' - ' + response.errorMessage,
                            type: AlertType.Danger,
                            dismissable: true,
                        }
                    }
                }
            );
        }
    }

    async formSubmitted(event: Event) {
        await this.roundStatusService
            .update(this.roundStatus!)
            .then(response => {
                    if (response.statusCode >= 200 && response.statusCode < 300) {
                        this._alert = null;
                        this.router.navigateToRoute('roundStatuses-index', {});
                    } else {
                        // show error message
                        this._alert = {
                            message: response.statusCode.toString() + ' - ' + response.errorMessage,
                            type: AlertType.Danger,
                            dismissable: true,
                        }
                    }
                }
            );
        event.preventDefault();
    }

}
