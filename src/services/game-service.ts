import {autoinject} from 'aurelia-framework';
import {HttpClient} from 'aurelia-fetch-client';
import {AppState} from "../state/app-state";
import {IFetchResponse} from "../types/IFetchResponse";
import {IGameDataView} from "../domain/view/IGameDataView";
import {IStateView} from "../domain/view/IStateView";
import {IInitReqView} from "../domain/view/IInitReqView";

const STATE_URL = 'STATE'; // get
const INIT_URL = 'INIT'; // post
const BET_URL = 'BET'; // post
const RESOLVE_URL = 'RESOLVE'; // post
const GAMEDATA_URL = 'GAMEDATA'; // get

@autoinject
export class GameService {

    constructor(private routeUrl: string, private httpClient: HttpClient, private appState: AppState) {
    }

    async getGameData(): Promise<IFetchResponse<IGameDataView>> {
        return await this.httpClient.get(GAMEDATA_URL, {
            cache: "no-store", headers: {authorization: "Bearer " + this.appState.JWT}
        })
            .then(async response => { // happy case
                if (response.status >= 200 && response.status < 300) {
                    const data = (await response.json()) as IGameDataView;
                    return {
                        statusCode: response.status,
                        data: data
                    }
                }
                const errMsg = !response.ok ? await response.text() : await response.json();
                return { // something went wrong
                    statusCode: response.status,
                    errorMessage: response.statusText + (errMsg ? " " + JSON.stringify(errMsg) : "")
                }
            })
            .catch(reason => { // request failed (e.g connection loss)
                return {
                    statusCode: 0,
                    errorMessage: JSON.stringify(reason)
                }
            })
    }

    async getState(): Promise<IFetchResponse<IStateView>> {
        return await this.httpClient.get(STATE_URL, {
            cache: "no-store", headers: {authorization: "Bearer " + this.appState.JWT}
        })
            .then(async response => { // happy case
                if (response.status >= 200 && response.status < 300) {
                    const data = (await response.json()) as IStateView;
                    return {
                        statusCode: response.status,
                        data: data
                    }
                }
                const errMsg = !response.ok ? await response.text() : await response.json();
                return { // something went wrong
                    statusCode: response.status,
                    errorMessage: response.statusText + (errMsg ? " " + JSON.stringify(errMsg) : "")
                }
            })
            .catch(reason => { // request failed (e.g connection loss)
                return {
                    statusCode: 0,
                    errorMessage: JSON.stringify(reason)
                }
            })
    }


    async init(data: IInitReqView): Promise<IFetchResponse<IStateView>> {
        return await this.httpClient.post(INIT_URL, JSON.stringify(data), {
            cache: "no-store", headers: {authorization: "Bearer " + this.appState.JWT}
        })
            .then(async response => { // happy case
                if (response.status >= 200 && response.status < 300) {
                    const data = (await response.json()) as IStateView;
                    return {
                        statusCode: response.status,
                        data: data
                    }
                }
                const errMsg = !response.ok ? await response.text() : await response.json();
                return { // something went wrong
                    statusCode: response.status,
                    errorMessage: response.statusText + (errMsg ? " " + JSON.stringify(errMsg) : "")
                }
            })
            .catch(reason => { // request failed (e.g connection loss)
                return {
                    statusCode: 0,
                    errorMessage: JSON.stringify(reason)
                }
            })
    }

    async bet(): Promise<IFetchResponse<IStateView>> {
        return await this.httpClient.post(BET_URL, null, {
            cache: "no-store", headers: {authorization: "Bearer " + this.appState.JWT}
        })
            .then(async response => { // happy case
                if (response.status >= 200 && response.status < 300) {
                    const data = (await response.json()) as IStateView;
                    return {
                        statusCode: response.status,
                        data: data
                    }
                }
                const errMsg = !response.ok ? await response.text() : await response.json();
                return { // something went wrong
                    statusCode: response.status,
                    errorMessage: response.statusText + (errMsg ? " " + JSON.stringify(errMsg) : "")
                }
            })
            .catch(reason => { // request failed (e.g connection loss)
                return {
                    statusCode: 0,
                    errorMessage: JSON.stringify(reason)
                }
            })
    }

    async resolve(): Promise<IFetchResponse<IStateView>> {
        return await this.httpClient.post(RESOLVE_URL, null, {
            cache: "no-store", headers: {authorization: "Bearer " + this.appState.JWT}
        })
            .then(async response => { // happy case
                if (response.status >= 200 && response.status < 300) {
                    const data = (await response.json()) as IStateView;
                    return {
                        statusCode: response.status,
                        data: data
                    }
                }
                const errMsg = !response.ok ? await response.text() : await response.json();
                return { // something went wrong
                    statusCode: response.status,
                    errorMessage: response.statusText + (errMsg ? " " + JSON.stringify(errMsg) : "")
                }
            })
            .catch(reason => { // request failed (e.g connection loss)
                return {
                    statusCode: 0,
                    errorMessage: JSON.stringify(reason)
                }
            })
    }
}
