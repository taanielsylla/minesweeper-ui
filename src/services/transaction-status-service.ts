import {autoinject} from 'aurelia-framework';
import {HttpClient} from 'aurelia-fetch-client';
import {AppState} from "../state/app-state";
import {BaseService} from "./base-service";
import {ITransactionStatus} from "../domain/ITransactionStatus";

const ROUTE_URL = 'TransactionStatuses';

@autoinject
export class TransactionStatusService extends BaseService<ITransactionStatus> {

    constructor(httpClient: HttpClient, appState: AppState) {
        super(ROUTE_URL, httpClient, appState);
    }

    // add custom methods
}
