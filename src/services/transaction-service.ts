import {autoinject} from 'aurelia-framework';
import {HttpClient} from 'aurelia-fetch-client';
import {AppState} from "../state/app-state";
import {BaseService} from "./base-service";
import {ITransaction} from "../domain/ITransaction";
import {IFetchResponse} from "../types/IFetchResponse";

const ROUTE_URL = 'Transactions';

@autoinject
export class TransactionService extends BaseService<ITransaction> {

    constructor(httpClient: HttpClient, appState: AppState) {
        super(ROUTE_URL, httpClient, appState);
    }

    async create(data: ITransaction): Promise<IFetchResponse<string>> {
        data.amount = parseFloat(data.amount.toString());
        data.endingBalance = parseFloat(data.endingBalance.toString());
        return super.create(data);
    }

    async update(data: ITransaction): Promise<IFetchResponse<string>> {
        data.amount = parseFloat(data.amount.toString());
        data.endingBalance = parseFloat(data.endingBalance.toString());
        return super.update(data);
    }

    // add custom methods
}
