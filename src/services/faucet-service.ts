import {autoinject} from 'aurelia-framework';
import {HttpClient} from 'aurelia-fetch-client';
import {AppState} from "../state/app-state";
import {BaseService} from "./base-service";
import {IFaucet} from "../domain/IFaucet";
import {IFetchResponse} from "../types/IFetchResponse";

const ROUTE_URL = 'Faucets';

@autoinject
export class FaucetService extends BaseService<IFaucet> {

    constructor(httpClient: HttpClient, appState: AppState) {
        super(ROUTE_URL, httpClient, appState);
    }

    async create(data: IFaucet): Promise<IFetchResponse<string>> {
        data.amountRetrieved = parseFloat(data.amountRetrieved.toString());
        return super.create(data);
    }

    async update(data: IFaucet): Promise<IFetchResponse<string>> {
        data.amountRetrieved = parseFloat(data.amountRetrieved.toString());
        return super.update(data);
    }

    // add custom methods
}
