import {autoinject} from 'aurelia-framework';
import {HttpClient} from 'aurelia-fetch-client';
import {AppState} from "../state/app-state";
import {BaseService} from "./base-service";
import {IRoundStatus} from "../domain/IRoundStatus";

const ROUTE_URL = 'RoundStatuses';

@autoinject
export class RoundStatusService extends BaseService<IRoundStatus> {

    constructor(httpClient: HttpClient, appState: AppState) {
        super(ROUTE_URL, httpClient, appState);
    }

    // add custom methods
}
