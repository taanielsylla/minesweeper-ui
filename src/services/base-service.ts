import {autoinject} from 'aurelia-framework';
import {HttpClient} from 'aurelia-fetch-client';
import {AppState} from "../state/app-state";
import {IFetchResponse} from "../types/IFetchResponse";

let ROUTE_URL: string;

@autoinject
export class BaseService<TType extends any> {

    protected httpClient: HttpClient;
    protected appState: AppState;

    constructor(routeUrl: string, httpClient: HttpClient, appState: AppState) {
        ROUTE_URL = routeUrl;
        this.appState = appState;
        this.httpClient = httpClient;
    }

    async getAll(): Promise<IFetchResponse<TType[]>> {
        return await this.httpClient.fetch(ROUTE_URL, {
            cache: "no-store", headers: {authorization: "Bearer " + this.appState.JWT}
        })
            .then(async response => { // happy case
                if (response.status >= 200 && response.status < 300) {
                    const data = (await response.json()) as TType[];
                    return {
                        statusCode: response.status,
                        data: data
                    }
                }
                return { // something went wrong
                    statusCode: response.status,
                    errorMessage: response.statusText
                }
            })
            .catch(reason => { // request failed (e.g connection loss)
                return {
                    statusCode: 0,
                    errorMessage: JSON.stringify(reason)
                }
            })
    }

    async getById(id: string): Promise<IFetchResponse<TType>> {
        return await this.httpClient.fetch(ROUTE_URL + '/' + id, {
            cache: "no-store", headers: {authorization: "Bearer " + this.appState.JWT}
        })
            .then(async response => { // happy case
                if (response.status >= 200 && response.status < 300) {
                    const data = (await response.json()) as TType;
                    return {
                        statusCode: response.status,
                        data: data
                    }
                }
                return { // something went wrong
                    statusCode: response.status,
                    errorMessage: response.statusText
                }
            })
            .catch(reason => { // request failed (e.g connection loss)
                return {
                    statusCode: 0,
                    errorMessage: JSON.stringify(reason)
                }
            })
    }

    async create(data: TType): Promise<IFetchResponse<string>> {
        return await this.httpClient.post(ROUTE_URL, JSON.stringify(data), {
            cache: "no-store", headers: {authorization: "Bearer " + this.appState.JWT}
        })
            .then(async response => { // happy case
                if (response.status >= 200 && response.status < 300) {
                    return {
                        statusCode: response.status
                        // no data
                    }
                }
                return { // something went wrong
                    statusCode: response.status,
                    errorMessage: response.statusText
                }
            })
            .catch(reason => { // request failed (e.g connection loss)
                return {
                    statusCode: 0,
                    errorMessage: JSON.stringify(reason)
                }
            })
    }

    async update(data: any): Promise<IFetchResponse<string>> {
        return await this.httpClient.put(ROUTE_URL + '/' + data.id, JSON.stringify(data), {
            cache: "no-store", headers: {authorization: "Bearer " + this.appState.JWT}
        })
            .then(async response => { // happy case
                if (response.status >= 200 && response.status < 300) {
                    return {
                        statusCode: response.status
                        // no data
                    }
                }
                return { // something went wrong
                    statusCode: response.status,
                    errorMessage: response.statusText
                }
            })
            .catch(reason => { // request failed (e.g connection loss)
                return {
                    statusCode: 0,
                    errorMessage: JSON.stringify(reason)
                }
            })
    }

    async delete(id: string): Promise<IFetchResponse<string>> {
        return await this.httpClient.delete(ROUTE_URL + '/' + id, null, {
            cache: "no-store", headers: {authorization: "Bearer " + this.appState.JWT}
        })
            .then(async response => { // happy case
                if (response.status >= 200 && response.status < 300) {
                    return {
                        statusCode: response.status
                        // no data
                    }
                }
                return { // something went wrong
                    statusCode: response.status,
                    errorMessage: response.statusText
                }
            })
            .catch(reason => { // request failed (e.g connection loss)
                return {
                    statusCode: 0,
                    errorMessage: JSON.stringify(reason)
                }
            })
    }

}
