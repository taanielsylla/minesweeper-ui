import {autoinject} from 'aurelia-framework';
import {HttpClient} from 'aurelia-fetch-client';
import {AppState} from "../state/app-state";
import {BaseService} from "./base-service";
import {IWallet} from "../domain/IWallet";
import {IFetchResponse} from "../types/IFetchResponse";

const ROUTE_URL = 'Wallets';

@autoinject
export class WalletService extends BaseService<IWallet> {

    constructor(httpClient: HttpClient, appState: AppState) {
        super(ROUTE_URL, httpClient, appState);
    }

    async create(data: IWallet): Promise<IFetchResponse<string>> {
        data.amount = parseFloat(data.amount.toString());
        return super.create(data);
    }

    async update(data: IWallet): Promise<IFetchResponse<string>> {
        data.amount = parseFloat(data.amount.toString());
        return super.update(data);
    }

// add custom methods
}
