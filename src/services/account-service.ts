import {autoinject} from 'aurelia-framework';
import {AppState} from "../state/app-state";
import {HttpClient} from "aurelia-fetch-client";
import {IFetchResponse} from "../types/IFetchResponse";
import {ILoginResponse} from "../domain/ILoginResponse";
import {IRegister} from "../domain/IRegister";


@autoinject
export class AccountService {

    constructor(private appState: AppState, private httpClient: HttpClient) {
        this.httpClient.baseUrl = this.appState.BASE_URL;
    }

    async login(email: string, password: string): Promise<IFetchResponse<ILoginResponse>> {
        try {
            const res = await this.httpClient.post('account/login', JSON.stringify({
                email: email,
                password: password,
            }), {
                cache: 'no-store'
            })

            // happy case
            if (res.status >= 200 && res.status < 300) {
                const data = (await res.json()) as ILoginResponse;
                return {
                    statusCode: res.status,
                    data: data
                }
            }

            // something went wrong
            return {
                statusCode: res.status,
                errorMessage: res.statusText
            }

        } catch (reason) {
            return {
                statusCode: 0,
                errorMessage: JSON.stringify(reason)
            }
        }
    }

    async register(data: IRegister): Promise<IFetchResponse<ILoginResponse>> {
        try {
            const res = await this.httpClient.post('account/register', JSON.stringify(data), {cache: 'no-store'})
            // happy case
            if (res.status >= 200 && res.status < 300) {
                const data = (await res.json()) as ILoginResponse;
                return {
                    statusCode: res.status,
                    data: data
                }
            }
            // something went wrong
            return {
                statusCode: res.status,
                errorMessage: res.statusText
            }

        } catch (reason) {
            return {
                statusCode: 0,
                errorMessage: JSON.stringify(reason)
            }
        }
    }

}
