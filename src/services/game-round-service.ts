import {autoinject} from 'aurelia-framework';
import {HttpClient} from 'aurelia-fetch-client';
import {AppState} from "../state/app-state";
import {BaseService} from "./base-service";
import {IGameRound} from "../domain/IGameRound";
import {IFetchResponse} from "../types/IFetchResponse";

const ROUTE_URL = 'GameRounds';

@autoinject
export class GameRoundService extends BaseService<IGameRound> {

    constructor(httpClient: HttpClient, appState: AppState) {
        super(ROUTE_URL, httpClient, appState);
    }

    async create(data: IGameRound): Promise<IFetchResponse<string>> {
        data.currentLevel = parseFloat(data.currentLevel.toString());
        return super.create(data);
    }

    async update(data: IGameRound): Promise<IFetchResponse<string>> {
        data.currentLevel = parseFloat(data.currentLevel.toString());
        return super.update(data);
    }

    // add custom methods
}
