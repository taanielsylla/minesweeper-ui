import {autoinject} from 'aurelia-framework';
import {HttpClient} from 'aurelia-fetch-client';
import {AppState} from "../state/app-state";
import {BaseService} from "./base-service";
import {ITransactionType} from "../domain/ITransactionType";

const ROUTE_URL = 'TransactionTypes';

@autoinject
export class TransactionTypeService extends BaseService<ITransactionType> {

    constructor(httpClient: HttpClient, appState: AppState) {
        super(ROUTE_URL, httpClient, appState);
    }

    // add custom methods
}
