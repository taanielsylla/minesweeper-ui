import {autoinject} from 'aurelia-framework';
import {HttpClient} from 'aurelia-fetch-client';
import {AppState} from "../state/app-state";
import {BaseService} from "./base-service";
import {IGameMode} from "../domain/IGameMode";
import {IFetchResponse} from "../types/IFetchResponse";

const ROUTE_URL = 'GameModes';

@autoinject
export class GameModeService extends BaseService<IGameMode> {

    constructor(httpClient: HttpClient, appState: AppState) {
        super(ROUTE_URL, httpClient, appState);
    }

    async create(data: IGameMode): Promise<IFetchResponse<string>> {
        data.countOfLevels = parseFloat(data.countOfLevels.toString());
        return super.create(data);
    }

    async update(data: IGameMode): Promise<IFetchResponse<string>> {
        data.countOfLevels = parseFloat(data.countOfLevels.toString());
        return super.update(data);
    }

    // add custom methods
}
