import {autoinject} from 'aurelia-framework';
import {HttpClient} from 'aurelia-fetch-client';
import {IAffiliate} from "../domain/IAffiliate";
import {AppState} from "../state/app-state";
import {BaseService} from "./base-service";

const ROUTE_URL = 'Affiliates';

@autoinject
export class AffiliateService extends BaseService<IAffiliate> {

    constructor(httpClient: HttpClient, appState: AppState) {
        super(ROUTE_URL, httpClient, appState);
    }

    // add custom methods
}
