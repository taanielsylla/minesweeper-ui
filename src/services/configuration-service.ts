import {autoinject} from 'aurelia-framework';
import {HttpClient} from 'aurelia-fetch-client';
import {IConfiguration} from "../domain/IConfiguration";
import {AppState} from "../state/app-state";
import {BaseService} from "./base-service";

const ROUTE_URL = 'Configurations';

@autoinject
export class ConfigurationService extends BaseService<IConfiguration> {

    constructor(httpClient: HttpClient, appState: AppState) {
        super(ROUTE_URL, httpClient, appState);
    }

    // add custom methods
}
