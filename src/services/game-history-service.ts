import {autoinject} from 'aurelia-framework';
import {HttpClient} from 'aurelia-fetch-client';
import {AppState} from "../state/app-state";
import {BaseService} from "./base-service";
import {IGameHistory} from "../domain/IGameHistory";
import {IFetchResponse} from "../types/IFetchResponse";

const ROUTE_URL = 'GameHistories';

@autoinject
export class GameHistoryService extends BaseService<IGameHistory> {

    constructor(httpClient: HttpClient, appState: AppState) {
        super(ROUTE_URL, httpClient, appState);
    }

    async create(data: IGameHistory): Promise<IFetchResponse<string>> {
        data.bet = parseFloat(data.bet.toString());
        data.win = parseFloat(data.win.toString());
        return super.create(data);
    }

    async update(data: IGameHistory): Promise<IFetchResponse<string>> {
        data.bet = parseFloat(data.bet.toString());
        data.win = parseFloat(data.win.toString());
        return super.update(data);
    }

    // add custom methods
}
