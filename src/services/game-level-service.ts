import {autoinject} from 'aurelia-framework';
import {HttpClient} from 'aurelia-fetch-client';
import {AppState} from "../state/app-state";
import {BaseService} from "./base-service";
import {IGameLevel} from "../domain/IGameLevel";
import {IFetchResponse} from "../types/IFetchResponse";

const ROUTE_URL = 'GameLevels';

@autoinject
export class GameLevelService extends BaseService<IGameLevel> {

    constructor(httpClient: HttpClient, appState: AppState) {
        super(ROUTE_URL, httpClient, appState);
    }

    async create(data: IGameLevel): Promise<IFetchResponse<string>> {
        data.bet = parseFloat(data.bet.toString());
        data.boxCount = parseFloat(data.boxCount.toString());
        data.levelNr = parseFloat(data.levelNr.toString());
        return super.create(data);
    }

    async update(data: IGameLevel): Promise<IFetchResponse<string>> {
        data.bet = parseFloat(data.bet.toString());
        data.boxCount = parseFloat(data.boxCount.toString());
        data.levelNr = parseFloat(data.levelNr.toString());
        return super.update(data);
    }

    // add custom methods
}
