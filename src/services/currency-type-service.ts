import {autoinject} from 'aurelia-framework';
import {HttpClient} from 'aurelia-fetch-client';
import {AppState} from "../state/app-state";
import {BaseService} from "./base-service";
import {ICurrencyType} from "../domain/ICurrencyType";

const ROUTE_URL = 'CurrencyTypes';

@autoinject
export class CurrencyTypeService extends BaseService<ICurrencyType> {

    constructor(httpClient: HttpClient, appState: AppState) {
        super(ROUTE_URL, httpClient, appState);
    }

    // add custom methods
}
