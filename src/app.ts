import {autoinject, PLATFORM} from 'aurelia-framework';
import {RouterConfiguration, Router, RouteConfig} from 'aurelia-router';
import * as _ from "lodash";
import {AppState} from "./state/app-state";
import {AccountService} from "./services/account-service";

@autoinject
export class App {
    router?: Router;

    private currentYear = new Date().getFullYear();

    constructor(
        private appState: AppState,
        accountService: AccountService, // adding this here so that base urs is set for HttpClient
    ) {

    }

    onLogoutClick() {
        this.appState.JWT = null;
        this.router!.navigateToRoute('account-login');
    }

    configureRouter(config: RouterConfiguration, router: Router): void {
        this.router = router;
        config.title = 'Minesweeper';

        const routes: RouteConfig[] = [
            {
                route: ['', 'home', 'home/index'], name: 'home-index', moduleId:
                    PLATFORM.moduleName('views/home/index'), nav: true, title: 'Home'
            },
            {
                route: ['account/login'], name: 'account-login', moduleId:
                    PLATFORM.moduleName('views/account/login'), nav: true, title: 'Login'
            },
            {
                route: ['account/register'], name: 'account-register', moduleId:
                    PLATFORM.moduleName('views/account/register'), nav: true, title: 'Register'
            }
        ];

        // iterate CRUD pages for api endpoints
        [
            "affiliates", "configurations", "currencyTypes", "faucets", "gameHistories", "gameLevels", "gameModes",
            "gameRounds", "roundStatuses", "transactions", "transactionStatuses", "transactionTypes", "wallets"
        ].forEach((routeName: string) => {

            const pascalCaseName = _.upperFirst(routeName); // TransactionType
            routes.push(
                {
                    route: [`${routeName}`, `${routeName}/index`], name: `${routeName}-index`, moduleId:
                        PLATFORM.moduleName(`views/${routeName}/index`), nav: true,
                    title: `${pascalCaseName}`
                },
                {
                    route: [`${routeName}/details/:id`], name: `${routeName}-details`, moduleId:
                        PLATFORM.moduleName(`views/${routeName}/details`), nav: false,
                    title: `${pascalCaseName} Details`
                },
                {
                    route: [`${routeName}/edit/:id`], name: `${routeName}-edit`, moduleId:
                        PLATFORM.moduleName(`views/${routeName}/edit`), nav: false,
                    title: `${pascalCaseName} Edit`
                },
                {
                    route: [`${routeName}/create`], name: `${routeName}-create`, moduleId:
                        PLATFORM.moduleName(`views/${routeName}/create`), nav: false,
                    title: `${pascalCaseName} Create`
                },
                {
                    route: [`${routeName}/delete/:id`], name: `${routeName}-delete`, moduleId:
                        PLATFORM.moduleName(`views/${routeName}/delete`), nav: false,
                    title: `${pascalCaseName} Delete`
                },
            );
        });

        // spec all dynamically generated route, this is for webpack tree-shaking.
        PLATFORM.moduleName('views/affiliates/index');
        PLATFORM.moduleName('views/affiliates/details');
        PLATFORM.moduleName('views/affiliates/edit');
        PLATFORM.moduleName('views/affiliates/create');
        PLATFORM.moduleName('views/affiliates/delete');

        PLATFORM.moduleName('views/configurations/index');
        PLATFORM.moduleName('views/configurations/details');
        PLATFORM.moduleName('views/configurations/edit');
        PLATFORM.moduleName('views/configurations/create');
        PLATFORM.moduleName('views/configurations/delete');

        PLATFORM.moduleName('views/currencyTypes/index');
        PLATFORM.moduleName('views/currencyTypes/details');
        PLATFORM.moduleName('views/currencyTypes/edit');
        PLATFORM.moduleName('views/currencyTypes/create');
        PLATFORM.moduleName('views/currencyTypes/delete');

        PLATFORM.moduleName('views/faucets/index');
        PLATFORM.moduleName('views/faucets/details');
        PLATFORM.moduleName('views/faucets/edit');
        PLATFORM.moduleName('views/faucets/create');
        PLATFORM.moduleName('views/faucets/delete');

        PLATFORM.moduleName('views/gameHistories/index');
        PLATFORM.moduleName('views/gameHistories/details');
        PLATFORM.moduleName('views/gameHistories/edit');
        PLATFORM.moduleName('views/gameHistories/create');
        PLATFORM.moduleName('views/gameHistories/delete');

        PLATFORM.moduleName('views/gameLevels/index');
        PLATFORM.moduleName('views/gameLevels/details');
        PLATFORM.moduleName('views/gameLevels/edit');
        PLATFORM.moduleName('views/gameLevels/create');
        PLATFORM.moduleName('views/gameLevels/delete');

        PLATFORM.moduleName('views/gameModes/index');
        PLATFORM.moduleName('views/gameModes/details');
        PLATFORM.moduleName('views/gameModes/edit');
        PLATFORM.moduleName('views/gameModes/create');
        PLATFORM.moduleName('views/gameModes/delete');

        PLATFORM.moduleName('views/gameRounds/index');
        PLATFORM.moduleName('views/gameRounds/details');
        PLATFORM.moduleName('views/gameRounds/edit');
        PLATFORM.moduleName('views/gameRounds/create');
        PLATFORM.moduleName('views/gameRounds/delete');

        PLATFORM.moduleName('views/roundStatuses/index');
        PLATFORM.moduleName('views/roundStatuses/details');
        PLATFORM.moduleName('views/roundStatuses/edit');
        PLATFORM.moduleName('views/roundStatuses/create');
        PLATFORM.moduleName('views/roundStatuses/delete');

        PLATFORM.moduleName('views/transactions/index');
        PLATFORM.moduleName('views/transactions/details');
        PLATFORM.moduleName('views/transactions/edit');
        PLATFORM.moduleName('views/transactions/create');
        PLATFORM.moduleName('views/transactions/delete');

        PLATFORM.moduleName('views/transactionStatuses/index');
        PLATFORM.moduleName('views/transactionStatuses/details');
        PLATFORM.moduleName('views/transactionStatuses/edit');
        PLATFORM.moduleName('views/transactionStatuses/create');
        PLATFORM.moduleName('views/transactionStatuses/delete');

        PLATFORM.moduleName('views/transactionTypes/index');
        PLATFORM.moduleName('views/transactionTypes/details');
        PLATFORM.moduleName('views/transactionTypes/edit');
        PLATFORM.moduleName('views/transactionTypes/create');
        PLATFORM.moduleName('views/transactionTypes/delete');

        PLATFORM.moduleName('views/wallets/index');
        PLATFORM.moduleName('views/wallets/details');
        PLATFORM.moduleName('views/wallets/edit');
        PLATFORM.moduleName('views/wallets/create');
        PLATFORM.moduleName('views/wallets/delete');


        // apply all routes
        config.map(routes);
        config.mapUnknownRoutes('views/home/index');
    }

}
