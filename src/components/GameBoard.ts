import {autoinject, bindable} from 'aurelia-framework';
import {IConfigurationView} from "../domain/view/IConfigurationView";
import {CurrencyManager} from "./CurrencyManager";

@autoinject
export class GameBoard {

    config?: IConfigurationView;
    parent?: HTMLDivElement;

    constructor(parent: HTMLDivElement, config?: IConfigurationView, cb?: Function) {
        this.parent = parent;
        this.config = config;

        if (this.config && cb) {
            const grid = this.createBoard(cb);
            this.parent.append(grid);
        }
    }

    private createBoard(cb: Function) {
        const rowCount = this.config.bets.length;
        // const rowCount = 3;
        const colCount = this.config.boxCount;
        // const colCount = 10;

        let i = 0;
        let grid = document.createElement('table');
        grid.className = 'grid';

        for (let r = 0; r < rowCount; ++r) {
            let tr = grid.appendChild(document.createElement('tr'));
            tr.classList.add("disabled"); // initially disabled

            for (let c = 0; c < colCount; ++c) {
                let cellContainer = document.createElement('td');
                let cell = cellContainer.appendChild(document.createElement('div'));

                cell.classList.add("cell");
                cell.dataset.row = (rowCount - r).toString();
                cell.dataset.col = (c + 1).toString();

                tr.appendChild(cellContainer);

                const cellBetAmount = [...this.config.bets].reverse()[r]
                const cellBetText = CurrencyManager.format(cellBetAmount);
                cell.dataset.bet = cellBetAmount.toString();
                cell.innerText = cellBetText;
                // cell.innerText = CurrencyManager.format(1648.64)
                ++i;

                cell.addEventListener('click', (function (el, r, c, i) {
                    return function () {
                        cb(el, c, r, i);
                    }
                })(cell, r, c, i), false);
            }
        }
        return grid;
    }

    setState() {

    }

    openToRow(currentLevel: number) {
        const rowCount = this.config.bets.length - 1;

        [...Array(currentLevel)].forEach((_, i) => {
            const rowElem = document.getElementById('gridContainer').getElementsByTagName('tr')[rowCount - i];
            rowElem.classList.remove('opened');
            rowElem.classList.add('open');
        })

        // Make top row selectable for player
        if (currentLevel < this.config.bets.length) {
            const rowAbove = document.getElementById('gridContainer').getElementsByTagName('tr')[rowCount - currentLevel];
            rowAbove.classList.remove("disabled");
        }

    }
}
