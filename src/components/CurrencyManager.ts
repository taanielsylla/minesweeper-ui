import {autoinject, bindable} from 'aurelia-framework';

@autoinject
export class CurrencyManager {

    static currencyName: string;
    static currencySymbol: string;

    static symbols: object = {
        BTC: "BTC",
        mBTC: "mBTC",
        Satoshi: "Satoshi",
    };

    static updateSelectedCurrency(currencyName: string) {
        CurrencyManager.currencyName = currencyName;
        CurrencyManager.currencySymbol = CurrencyManager.symbols[currencyName];
    }

    static format(amount: number): string {
        return `${amount}
        ${CurrencyManager.currencySymbol}`
    }
}
